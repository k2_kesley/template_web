Recarga de Celular

Narrativa:
Para poder recarregar o celular
Como um consumidor
Eu quero conseguir criar um novo contato

Cenario: O consumidor deve conseguir criar um novo contato para recarga
Dado que eu acesse o Calsystem
Quando eu logo no sistema
Então eu devo ver a tela inicial
Quando eu acesso a funcao recarga de celular
Então eu devo ver a barra de busca de CPF
Quando eu busco pelo CPF do cliente
Então eu devo ver a lista de contas do cliente
Quando eu clico em uma conta
Então eu devo ver a tela de contatos
Quando eu clico no botao criar contato
Então eu devo ver a tela de criacao de novo contato
Quando eu o formulario, ativo a opcao salvar numero e concluo o cadastro
Então eu devo ver o novo contato na lista