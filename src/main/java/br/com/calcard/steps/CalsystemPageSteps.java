package br.com.calcard.steps;

import br.com.calcard.pages.CalsystemHomePage;
import br.com.calcard.pages.CalsystemLoginPage;
import br.com.calcard.pages.menus.Estabelecimento;
import br.com.calcard.pages.menus.Usuario;
import br.com.calcard.utils.Utils;
import org.apache.log4j.Logger;
import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import static br.com.calcard.utils.Utils.*;

@Component
public class CalsystemPageSteps extends AbstractSteps {
    protected Logger LOG = Logger.getLogger(this.getClass());
    @Autowired
    protected WebDriverProvider webDriverProvider;
    @Autowired
    CalsystemHomePage calsystemHomePage;
    @Autowired
    CalsystemLoginPage calsystemLoginPage;
    @Autowired
    Estabelecimento estabelecimento;
    @Autowired
    Usuario usuario;
    @Autowired
    Utils utils;
    @Value("${home.url}")
    private String CALSYSTEM_HOME_URL;
    @Value("${CALSYSTEM_USERNAME}")
    private String CALSYSTEM_USER;
    @Value("${CALSYSTEM_PASSWORD}")
    private String CALSYSTEM_PASS;

    @AfterScenario
    public void logout(){
        logoutFromCalsystem(calsystemHomePage, usuario);
    }

    @Given("eu acesse o Calsystem")
    public void givenEuAcesseOCalsystem() {
        navigateTo(CALSYSTEM_HOME_URL);
    }

    @When("eu logo no sistema")
    public void whenEuLogoNoSistema() {
        waitForPageLoad();
        loginToCalsystem(calsystemLoginPage, CALSYSTEM_USER, CALSYSTEM_PASS);
    }

    @Then("eu devo ver a tela inicial")
    public void thenEuDevoVerATelaInicial() {
        waitForPageLoad();
    }

    @When("eu acesso a funcao recarga de celular")
    public void whenEuAcessoAFuncaoRecargaDeCelular() {
        click(calsystemHomePage.MENU_Estabelecimento);
        click(estabelecimento.alterarLimite);
    }

    @Then("eu devo ver a barra de busca de CPF")
    public void thenEuDevoVerABarraDeBuscaDeCPF() {
        // PENDENTE
    }

    @When("eu busco pelo CPF do cliente")
    public void whenEuBuscoPeloCPFDoCliente() {
        // PENDENTE
    }

    @Then("eu devo ver a lista de contas do cliente")
    public void thenEuDevoVerAListaDeContasDoCliente() {
        // PENDENTE
    }

    @When("eu clico em uma conta")
    public void whenEuClicoEmUmaConta() {
        // PENDENTE
    }

    @Then("eu devo ver a tela de contatos")
    public void thenEuDevoVerATelaDeContatos() {
        // PENDENTE
    }

    @When("eu clico no botao criar contato")
    public void whenEuClicoNoBotaoCriarContato() {
        // PENDENTE
    }

    @Then("eu devo ver a tela de criacao de novo contato")
    public void thenEuDevoVerATelaDeCriacaoDeNovoContato() {
        // PENDENTE
    }

    @When("eu preencho o formulario com os limites superiores, ativo a opcao salvar numero e concluo o cadastro")
    public void whenEuPreenchoOFormularioComOsLimitesSuperioresAtivoAOpcaoSalvarNumeroEConcluoOCadastro() {
        // PENDENTE
    }

    @Then("eu devo ver o novo contato na lista")
    public void thenEuDevoVerONovoContatoNaLista() {
        // PENDENTE
    }
}
