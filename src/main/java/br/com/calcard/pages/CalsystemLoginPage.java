package br.com.calcard.pages;

import br.com.calcard.configuration.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@PageObject
public class CalsystemLoginPage {
    @FindBy(id = "usuario")
    public WebElement loginField;
    @FindBy(id = "password")
    public WebElement passwordField;
    @FindBy(xpath = "//button[@type='submit']")
    public WebElement loginButton;
}
