package br.com.calcard.pages;

import br.com.calcard.configuration.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;

@Component
@PageObject
public class CalsystemHomePage {
    @FindBy(xpath = "//span[text()='Estabelecimento']")
    public WebElement MENU_Estabelecimento;
    @FindBy(xpath = "//span[text()='Mesa de Crédito']")
    public WebElement MENU_MesaDeCredito;
    @FindBy(xpath = "//span[text()='Configuração']")
    public WebElement MENU_Configuracao;
    @FindBy(xpath = "//span[text()='Financeiro']")
    public WebElement MENU_Financeiro;
    @FindBy(xpath = "//span[text()='Cartão']")
    public WebElement MENU_Cartao;
    @FindBy(xpath = "//span[text()='Crédito']")
    public WebElement MENU_Credito;
    @FindBy(xpath = "//span[text()='Impressora']")
    public WebElement MENU_Impressora;
    @FindBy(xpath = "//li[contains(@class,'user-profile') and a[i and span]]")
    public WebElement MENU_Usuario;
    @FindBy(xpath = "//li[contains(@class,'user-profile') and a[contains(@href,'welcome')]]")
    public WebElement MENU_Home;
    @FindBy(xpath = "//li[contains(@class,'user-profile') and a[contains(@href,'servicedesk')]]")
    public WebElement MENU_ServiceDesk;
    @FindBy(xpath = "//li[a[contains(@ng-click,'toggleNotifications')]]")
    public WebElement MENU_Notificacoes;
}
