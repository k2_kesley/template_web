package br.com.calcard.pages.menus;

import br.com.calcard.configuration.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;

@Component
@PageObject
public class Usuario {
    @FindBy(xpath = "//li[contains(@class,'user-profile') and a[i and span]]//li[contains(.,'Alterar senha')]")
    public WebElement alterarSenhaUsuario;
    @FindBy(xpath = "//li[contains(@class,'user-profile') and a[i and span]]//li[contains(.,'Sair')]")
    public WebElement sairUsuario;
}
