package br.com.calcard.pages.menus;

import br.com.calcard.configuration.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;

@Component
@PageObject
public class Estabelecimento {
    @FindBy(xpath = "//span[text()='Alterar Senha']")
    public WebElement alterarSenha;
    @FindBy(xpath = "//li[a[contains(.,'Consultar Pendências')]]")
    public WebElement consultarPendencias;
    @FindBy(xpath = "//li[a[contains(.,'Alterar Limite')]]")
    public WebElement alterarLimite;
    @FindBy(xpath = "//li[a[contains(.,'Reabilitação de Conta')]]")
    public WebElement reabilitacaoDeConta;
    @FindBy(xpath = "//li[a[contains(.,'Imprimir Cartão')]]")
    public WebElement imprimirCartao;
    @FindBy(xpath = "//li[a[contains(.,'Cadastrar Proposta')]]")
    public WebElement cadastrarProposta;
    @FindBy(xpath = "//li[a[contains(.,'Consultar termo de aceite')]]")
    public WebElement consultarTermoAceite;
    @FindBy(xpath = "//li[a[contains(.,'Migração Visa')]]")
    public WebElement migracaoVisa;
    @FindBy(xpath = "//li[a[contains(.,'Desbloqueio de cartão')]]")
    public WebElement desbloqueioCartao;
    @FindBy(xpath = "//li[a[contains(.,'Seguros e Assistências')]]")
    public WebElement segurosAssistencias;
    @FindBy(xpath = "//li[a[contains(.,'Reiniciar senha colaborador')]]")
    public WebElement reiniciarSenhaColaborador;
    @FindBy(xpath = "//li[a[contains(.,'Informações de Conta')]]")
    public WebElement informacoesDeConta;
}
