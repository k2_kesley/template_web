package br.com.calcard.utils;

import br.com.calcard.pages.CalsystemHomePage;
import br.com.calcard.pages.CalsystemLoginPage;
import br.com.calcard.pages.menus.Usuario;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component
public class Utils {
    protected static WebDriverProvider webDriverProvider;

    public Utils(WebDriverProvider webDriverProvider) {
        Utils.webDriverProvider = webDriverProvider;
    }

    /**
     * Makes the execution halt until the element is found. Polling for the element every 3 seconds with timeout of 15 seconds.
     *
     * @param by Identificador de tipo de busca (By.xpath, By.id, By.css, etc)
     * @return Return the webElement
     */
    public static WebElement waitForElement(By by) {
        Wait<WebDriver> wait = new FluentWait<>(webDriverProvider.get())
                .withTimeout(Duration.ofSeconds(20))
                .pollingEvery(Duration.ofSeconds(3))
                .ignoring(NoSuchElementException.class)
                .ignoring(StaleElementReferenceException.class);
        return wait.until(webDriver -> {
            if (webDriver == null) throw new AssertionError();
            return webDriver.findElement(by);
        });

    }

    /**
     * Navigates to a given URL
     *
     * @param url url to navigate to
     */
    public static void navigateTo(String url) {
        webDriverProvider.get().get(url);
    }

    /**
     * Clicks on a web element
     *
     * @param webElement  WebElement from selenium
     */
    public static void click(WebElement webElement) {
        webElement.click();
    }

    /**
     * Makes the execution halt until the condition of the element being clickable is met
     *
     * @param webElement  WebElement from selenium
     */
    public static void waitUntilClickable(WebElement webElement) {
        new WebDriverWait(webDriverProvider.get(), Duration.ofSeconds(15))
                .until(ExpectedConditions.elementToBeClickable(webElement));
    }

    /**
     * Makes the execution halt until the condition of the element being visible is met
     * Default timeout: 15 seconds
     */
    public static void waitUntilVisibility(WebElement webElement) {
        new WebDriverWait(webDriverProvider.get(), Duration.ofSeconds(15))
                .until(ExpectedConditions.visibilityOf(webElement));
    }

    /**
     * Makes the execution halt until the condition of the element being invisible is met
     * Default timeout: 15 seconds
     */
    public static void waitUntilInvisibility(WebElement webElement) {
        new WebDriverWait(webDriverProvider.get(), Duration.ofSeconds(30))
                .until(ExpectedConditions.invisibilityOf(webElement));
    }

    /**
     * Set text inside a web element
     *
     * @param webElement  WebElement from selenium
     * @param text text that should be entered into field
     */
    public static void setText(WebElement webElement, String text) {
        webElement.clear();
        webElement.sendKeys(text);
    }

    /**
     * 1
     * Gets text from a web element
     *
     * @param webElement  WebElement from selenium
     * @return returns text from webElement
     */
    public static String getText(WebElement webElement) {
        return webElement.getText();
    }

    /**
     * Get text from anything that can be identified using By
     *
     * @param by By locator (By.xpath, By.css, etc)
     * @return returns text from web element
     */
    public static String getText(By by) {
        return webDriverProvider.get().findElement(by).getText();
    }

    /**
     * Gets the value from an input
     *
     * @param webElement  WebElement from selenium
     * @return value of the input
     */
    public static String getTextFromInput(WebElement webElement) {
        return webElement.getAttribute("value").trim();
    }

    /** Validate if WebElement has text.
     * @param webElement  WebElement from selenium
     * @return returns true/false based on whether or not there's text on the web element
     */
    public static Boolean validateTextExists(WebElement webElement) {
        return !getText(webElement).isEmpty() && !getText(webElement).isBlank() && getText(webElement) != null;
    }

    /**
     * Makes the test halt for 10 seconds before executing the next step. Useful when explicit, implicit or fluente wait
     * can't be used.
     */
    public static void waitForPageLoad() {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Evaluates if the web element is selected.
     *
     * @param webElement  WebElement from selenium
     * @return returns true/false
     */
    public static Boolean isSelected(WebElement webElement) {
        return webElement.isSelected();
    }

    /**
     * Clear any text from any type of input/textarea.
     *
     * @param webElement  WebElement from selenium WebElement from selenium
     */
    public static void clearText(WebElement webElement) {
        click(webElement);
        webElement.sendKeys(Keys.CONTROL + "a");
        webElement.sendKeys(Keys.DELETE);
    }

    /** Scroll given element into view
     * @param by By locator (By.xpath, By.css, etc)
     */
    public static void scrollIntoView(By by) {
        WebElement element = webDriverProvider.get().findElement(by);
        ((JavascriptExecutor) webDriverProvider.get()).executeScript("arguments[0].scrollIntoView(true);", element);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Login to Calsystem using the values in the properties file.
     * @param calsystemLoginPage object mapping the login page
     * @param CALSYSTEM_USER calsystem user
     * @param CALSYSTEM_PASS calsystem password
     */
    public static void loginToCalsystem(CalsystemLoginPage calsystemLoginPage, String CALSYSTEM_USER, String CALSYSTEM_PASS){
        setText(calsystemLoginPage.loginField, CALSYSTEM_USER);
        setText(calsystemLoginPage.passwordField, CALSYSTEM_PASS);
        click(calsystemLoginPage.loginButton);
    }

    /**
     * Function used to wait for the visibility of all elements
     */
    public static void waitForVisibilityOfAllElements() {
        WebDriverWait wait = new WebDriverWait(webDriverProvider.get(), Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOfAllElements(webDriverProvider.get().findElements(By.xpath("//*"))));
    }

    /** Logout from Calsystem
     * @param calsystemHomePage object mapping the home page
     * @param usuario object mapping the user sub menu
     */
    public static void logoutFromCalsystem(CalsystemHomePage calsystemHomePage, Usuario usuario){
        click(calsystemHomePage.MENU_Usuario);
        click(usuario.sairUsuario);
    }
/*
    public static double getProductValue(WebElement webElement) throws ParseException {
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        Number number;
        number = nf.parse(webElement.getText().trim().strip());
        return number.doubleValue();
    }

    public static String getProductValueInCurrency(Double value) {
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        String number;
        number = nf.format(value);
        return number;
    }


    public static ArrayList<HashMap<String, String>> readDataFromDatasheet(String path) {
        ArrayList<HashMap<String, String>> dataset = new ArrayList<>();
        HashMap<String, String> data = new HashMap<>();
        try (InputStream inp = new FileInputStream(path)) {
            Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);
            sheet.removeRow(sheet.getRow(0));
            for (Row row : sheet) {
                for (Cell cell : row) {
                    if (cell.getColumnIndex() > 6)
                        break;
                    switch (cell.getCellType()) {
                        case STRING:
                            if (cell.getColumnIndex() == 0) {
                                data.put("username", cell.getStringCellValue());
                            } else if (cell.getColumnIndex() == 1) {
                                data.put("email", cell.getStringCellValue());
                            } else if (cell.getColumnIndex() == 2) {
                                data.put("password", cell.getStringCellValue());
                            } else if (cell.getColumnIndex() == 3) {
                                data.put("popularItem", cell.getStringCellValue());
                            } else if (cell.getColumnIndex() == 5) {
                                data.put("out _tracking_number", cell.getStringCellValue());
                            } else if (cell.getColumnIndex() == 6) {
                                data.put("out_order_number", cell.getStringCellValue());
                            }
                            break;
                        case NUMERIC:
                            data.put("quantity", String.valueOf(cell.getNumericCellValue()));
                            System.out.println(cell.getNumericCellValue());
                            break;
                    }
                }
                dataset.add(data);
            }
            System.out.println(dataset.toString());
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return dataset;
    }
*/

}